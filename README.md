# terraform-aws__lab

## Summary

(Work in progress)
Terraform codes to create a wordpress using ansible on a AWS EC2 instance.

## How to run

Change the AWS region in vars.tf to your preferred one.

Use the below commands to build, review and execute.
```
terraform init
terraform get
terraform plan
terraform apply
```