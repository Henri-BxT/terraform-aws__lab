output "vpc_id" {
  value = aws_vpc.main_vpc.id
}

output "public_subnet_id" {
  value = aws_subnet.eu-west-1a-public.id
}

output "private_subnet_id" {
  value = aws_subnet.eu-west-1a-private.id
}