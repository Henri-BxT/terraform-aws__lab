locals {
  vpc_cidr            = "10.0.0.0/16"
  public_subnet_cidr  = "10.0.0.0/24"
  private_subnet_cidr = "10.0.1.0/24"
}

resource "aws_vpc" "main_vpc" {
  cidr_block = local.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "terraform-aws-vpc"
  }
}

resource "aws_route_table" "eu-west-1a-public" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig-main.id
  }

  tags = {
    Name = "Public Subnet"
  }
}

resource "aws_route_table_association" "eu-west-1a-public" {
  route_table_id = aws_route_table.eu-west-1a-public.id
  subnet_id      = aws_subnet.eu-west-1a-public.id
}

resource "aws_subnet" "eu-west-1a-public" {
  vpc_id = aws_vpc.main_vpc.id

  cidr_block        = local.public_subnet_cidr
  availability_zone = "eu-west-1a"
  
  map_public_ip_on_launch = false

  tags = {
    Name = "Public subnet"
  }
}

resource "aws_subnet" "eu-west-1a-private" {
  vpc_id = aws_vpc.main_vpc.id

  cidr_block        = local.private_subnet_cidr
  availability_zone = "eu-west-1a"

  tags = {
    Name = "Private subnet"
  }
}

resource "aws_internet_gateway" "ig-main" {
  vpc_id = aws_vpc.main_vpc.id
}