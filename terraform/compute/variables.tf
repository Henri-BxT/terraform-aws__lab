variable "aws_region" {
  default = "eu-west-1"
}

variable "aws_key_name" {
  default = "ansible"
}

variable "instance_type" {
  default = "t3.medium"
}

variable "root_block_device_size" {
  default = 50
}

variable "root_block_device_type" {
  default = "gp2"
}