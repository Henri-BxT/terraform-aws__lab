output "web_id" {
  value = aws_instance.web-1.id
}


output "web_ip" {
  value = aws_instance.web-1.public_ip
}

output "db_ip" {
  value = aws_instance.web-1.private_ip
}

output "ansible_host_inventory" {
  value = <<EOF
web
db
EOF
}

output "ssh_config" {
  value = <<EOF
Host web
  Hostname ${aws_eip.lab.public_ip}

Host db
  Hostname ${aws_instance.web-1.private_ip}
  ProxyCommand ssh -F ssh.cfg -W %h:%p web

Host *
  IdentityFile keys/ansible.key
  User admin
  IdentitiesOnly yes
EOF
}