locals {
  private_key_file = "${path.module}/../../keys/ansible.key"
  public_key_file  = "${local.private_key_file}.pub"
}

resource "aws_key_pair" "ansible" {
  key_name   = var.aws_key_name
  public_key = file(local.public_key_file)
}

resource "aws_instance" "web-1" {
  ami                    = data.aws_ami.debian.id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.ansible.key_name
  vpc_security_group_ids = [aws_security_group.web.id]
  subnet_id              = data.terraform_remote_state.vpc.outputs.public_subnet_id
  source_dest_check      = false

  root_block_device {
    volume_size = var.root_block_device_size
    volume_type = var.root_block_device_type
  }

  user_data = <<-EOF
#cloud-config
repo_update: true
repo_upgrade: all

packages:
  - git
  - python3
  - python3-venv
  - python3-pip
  - sudo

runcmd:
  - pip3 install ansible
EOF

  tags = {
    Name = "Web server 1"
  }
}


resource "aws_eip" "lab" {
  vpc = true
}

resource "aws_eip_association" "lab" {
  instance_id   = aws_instance.web-1.id
  allocation_id = aws_eip.lab.id
}